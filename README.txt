# ASCII BD

## Pistes

- Le tour de France en ASCII ?
  + montagne ( 1 )          
  + volcan ( 1 )
  + forêt
  + temple de la forêt
  + ville ( 1 )
  + tokyo / new york
  + bourgade
  + desert
  + pyramide ( 1 )
  + plage
  + ferry
  + sous-marin
  + tunnel
  + grotte
  + pont
  + dragon ( 1 )
  + géant
  + orgre
  + démon
  + campagne ( 3-4 )
  + pyramide
  + temps ( 1 )
  + zoo
  + alien
  + enfer
  + nasa
  + port
  + mine
  + travaux chantier
  + ferme
  + champs
  + élevage
  + jungle
  + temple de la jungle
  + alice au pays des merveilles
  + marais
  + magiciens·nes
  + sorciers·ères
  + château
  + moyen-age
  + monastère
  + musée
  + lune
  + mars
  + espace
  + hollywood / tournage de cinéma
  + vikings / gaulois
  + neige
  + noël
  + ski
  + brume
  + nuit
  + road66
  + pré-histoire
  + cimetière
  + centrale nucléaire

## font

- unifont: 8*16
  + 76*58
    * 608*928
  + 86*64
    * 688*1024
- jgs: 9*18
  + 76*58
    * 684*1044 (haut/bas 18*2, externe 9*4, interne 9*8)

## commands

remove chars after 76e char

    :%s/\%>76c.*//
